# Neo-Colonialism OCR clean up

This is a repo for cleaning up an ebook ready version of Kwame Nkrumah's Neo-Colonialism: The Last Stage of Imperialism. There are many OCR artifacts left in the text currently. Merge requests and issues are very welcome. 

The HTML file will be the most up to date version, with and EPUB and AZW3 provided for those who just want a version for their eReader. You can convert the HTML to any format you like using Calibre.

## Useful clean-up heuristics

- Spell-check
- Searching for stray full-stops
  - regex: `\.(\ )*([a-z])`
- Searching for stray hyphens
  - regex: `-(\ )+([a-z])`
- Searching for wrongly capitalised letters
  - regex: `[^\.](\ )+([A-Z])`
  - High false positive rate likely
  - middle of word regex: `([a-z])([A-Z])`
